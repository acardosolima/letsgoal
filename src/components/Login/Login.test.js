import { render, screen } from "@testing-library/react";
import Login from "./Login";

test("renders dummy text at screen", () => {
  render(<Login />);

  const linkValue = screen.getByText("Login Page");
  expect(linkValue).toBeInTheDocument();
});
