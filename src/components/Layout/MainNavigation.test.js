import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import MainNavigation from "./MainNavigation";

test("renders home page link to user", () => {
  render(
    <BrowserRouter>
      <MainNavigation />
    </BrowserRouter>
  );

  const linkValue = screen.getByText("Home");
  expect(linkValue).toBeInTheDocument();
});

test("renders login page link to unauthenticated user", () => {
  render(
    <BrowserRouter>
      <MainNavigation />
    </BrowserRouter>
  );

  const linkValue = screen.getByText("Login");
  expect(linkValue).toBeInTheDocument();
});
