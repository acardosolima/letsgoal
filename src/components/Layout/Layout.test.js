import { render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Layout from "./Layout";

test("renders children component received via props", () => {
  render(
    <Layout>
      <h1>Layout</h1>
    </Layout>,
    { wrapper: BrowserRouter }
  );

  const linkValue = screen.getByText("Layout");
  expect(linkValue).toBeInTheDocument();
});
