import classes from "./Landing.module.css";

const Landing = () => {
  return (
    <section className={classes.landing}>
      <h1>Home Page</h1>
    </section>
  );
};

export default Landing;
