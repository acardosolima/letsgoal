import { render, screen } from "@testing-library/react";
import Landing from "./Landing";

test("renders dummy text at screen", () => {
  render(<Landing />);

  const linkValue = screen.getByText("Home Page");
  expect(linkValue).toBeInTheDocument();
});
