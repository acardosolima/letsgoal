import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { BrowserRouter, MemoryRouter } from "react-router-dom";

import App from "./App";

test("home page default rendering with success", () => {
  render(<App />, { wrapper: BrowserRouter });

  expect(screen.getByText(/Home Page/i)).toBeInTheDocument();
});

test("succesfull page navigation from home to login", async () => {
  render(<App />, { wrapper: BrowserRouter });

  await userEvent.click(screen.getByText(/Login/i));
  expect(screen.getByText(/Login Page/i)).toBeInTheDocument();
});

test("page not found redirected to home page", () => {
  const missingRoute = "/not_found";

  render(
    <MemoryRouter initialEntries={[missingRoute]}>
      <App />
    </MemoryRouter>
  );

  expect(screen.getByText(/Home Page/i)).toBeInTheDocument();
});
